# Website Builder

Build a tailored Docker image for website generator. Preinstalled are all requirements from 
develop branch of the [model-repo-parser](https://gitlab.com/morpheus.lab/model-repo-parser/-/tree/develop).
The resulting image is deployed at **registry.gitlab.com/morpheus/website-builder**.
