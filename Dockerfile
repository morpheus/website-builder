FROM registry.gitlab.com/pages/hugo/hugo_extended:HUGO_VERSION

RUN apk add -X https://dl-cdn.alpinelinux.org/alpine/v3.16/main -u alpine-keys --allow-untrusted
RUN apk upgrade
RUN apk add --update-cache curl git go python3 build-base python3-dev py3-pip py3-wheel py3-matplotlib py3-pandas
RUN curl https://gitlab.com/morpheus.lab/model-repo-parser/-/raw/master/requirements.txt -o requirements.txt
RUN pip install --upgrade pip
RUN pip install -r requirements.txt --prefer-binary